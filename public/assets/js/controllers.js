
//this controller will handle the initial page load, the home state
TSB.controllers.ListFilesCtrl = ['$scope', '$rootScope', '$location', 'APIService', '$state', '$stateParams', '$timeout', 'files', function($scope, $rootScope, $location, APIService, $state, $stateParams, $timeout, files){
	$scope.files = files.data.files;

	console.log($scope.files);

	$scope.fileMouseOver = function() {
		this.fileHover = true;
	}

	$scope.fileMouseLeave = function() {
		this.fileHover = false;
	}
}];



//this controller will handle the builder of settings files
TSB.controllers.CreateFileCtrl = ['$scope', '$rootScope', '$location', 'APIService', '$state', '$stateParams', '$timeout', '$sce', 'shop', function($scope, $rootScope, $location, APIService, $state, $stateParams, $timeout, $sce, shop){
	$scope.shop = {
		'themes': shop.data.themes.themes,
		'shop': shop.data.shop
	};

	$scope.file = {};
	$scope.file.sections = [];

	//console.log($scope.shop);

	$scope.fileMouseOver = function() {
		this.fileHover = true;
	}

	$scope.fileMouseLeave = function() {
		this.fileHover = false;
	}


	$scope.toggleThemeSelection = function() {
		$('#shopify_themes').slideToggle('fast');
	}

    $scope.addSection = function() {
    	var section = {name: $scope.section.name};
    	$scope.file.sections.push(section);
    }


	$scope.createFile = function() {

		//var file = getCurrentFile();
		var file = $scope.file;

		console.log(file);

		//update layout using our service
		APIService.createFile(file)
		.success(function(data){
			console.log(data);

			//updated successfully
			if(data.status == 'success')
			{
				//notify use

				if(data.activated) ShopifyApp.flashNotice('Settings file created and activated successfully.');
				else ShopifyApp.flashNotice('Settings file created successfully.');

				//$scope.setPreview($scope.theme.id);

				//redirect back to home view
				$state.go('list_files', {});
			}
			else
			{
				console.log('failed to create the layout');

				ShopifyApp.flashError(data.message);
			}

		})
		.error(function(error){
			console.log(error);
		});
	}

}];


//this controller will handle the before delete view
/*TSB.controllers.DeletePromoCtrl = ['$scope', '$rootScope', '$location', 'APIService', '$state', '$stateParams', '$timeout', function($scope, $rootScope, $location, APIService, $state, $stateParams, $timeout){
	//$scope.promotions = promotion.data.promotion;

	//console.log($scope.promotions);

	$scope.promotion_id = $state.params.promotion_id;


	$scope.deletePromotion = function(id) {

		//update layout using our service
		APIService.deletePromotion(id)
		.success(function(data){
			console.log(data);

			//updated successfully
			if(data.status == 'success')
			{
				//notify use

				ShopifyApp.flashNotice('Promo bar deleted successfully.');

				//redirect back to home view
				$state.go('home', {});
			}
			else
			{
				console.log('failed to delete the promo');

				ShopifyApp.flashError(data.message);
			}

		})
		.error(function(error){
			console.log(error);
		});
	}

}];


//this controller will handle updating an existing promo
TSB.controllers.UpdatePromoCtrl = ['$scope', '$rootScope', '$location', 'APIService', '$state', '$stateParams', '$timeout', '$sce', 'shop', 'promotion', function($scope, $rootScope, $location, APIService, $state, $stateParams, $timeout, $sce, shop, promotion){
	$scope.promotion = promotion.data.promotion;
	$scope.shop = {
		'themes': shop.data.themes.themes,
		'shop': shop.data.shop
	};


	//color picker
    $('.color-picker').colpick(
        {
            layout: 'hex',
            submit: 0,
            onChange:function(hsb, hex, rgb, el, bySetColor)
            {
                $(el).next().css('background-color', '#' + hex);
                if(!bySetColor)
                {
                    $(el).val(hex);
                }
            }
    }).on('keyup', function()
    {
        $(this).colpickSetColor(this.value);
    });
    $('.color-picker').each(function()
    {
       if ($(this).val())
       {
           $(this).colpickSetColor($(this).val());
       }
    });

    //datetime picker
    $('#scheduled_start_date').datetimepicker({
    	timepicker: true,
    	datepicker: true,
    	formateDate: 'Y-m-d H:i:s'
    });
    $('#scheduled_end_date').datetimepicker({
    	timepicker: true,
    	datepicker: true,
    	formateDate: 'Y-m-d H:i:s'
    });


	//console.log($scope.promotion);
	//console.log($scope.shop);

	$scope.previewLink = '';

	$scope.setPreview = function(theme_id) {

		if(theme_id) $scope.previewLink = $sce.trustAsResourceUrl('https://' + $scope.shop.shop.domain + '/?preview_theme_id=' + theme_id);
		else $scope.previewLink = $sce.trustAsResourceUrl('https://' + $scope.shop.shop.domain);

		//console.log($scope.previewLink);
	}
	$scope.setPreview(0);


	$scope.changeSelectedFont = function(){
		var font = $('select[name="font"]').val();
		$('select[name="font"]')[0].style = "font-family: '" + font + "', sans-serif !important;";
	}


	function getCurrentPromo(){
		var promo = {};
		promo.id = $scope.promotion.id;
		promo.text = $('input[name="text"]').val();
		promo.foreground_color = $('input[name="foreground_color"]').val();
		promo.background_color = $('input[name="background_color"]').val();
		promo.destination_url = $('input[name="destination_url"]').val();
		//promo.position = $('select[name="position"]').val();
		promo.is_sticky = $('input[name="is_sticky"]')[0].checked;
		promo.entrance_animation = $('select[name="entrance_animation"]').val();
		promo.font = $('select[name="font"]').val();
		promo.font_style = $('select[name="font_style"]').val();
		promo.in_newtab = $('input[name="in_newtab"]')[0].checked;
		promo.activate_now = $('input[name="is_active"]')[0].checked;
		promo.shopify_theme_id = typeof $scope.promotion.shopify_theme_id !== 'undefined' ? $scope.promotion.shopify_theme_id : 0;
		promo.scheduled_start_date = $scope.promotion.scheduled_start_date;
		promo.scheduled_end_date = $scope.promotion.scheduled_end_date;

		return promo;
	}


	$scope.updatePromotion = function() {

		var promo = getCurrentPromo();

		console.log(promo);

		//update layout using our service
		APIService.updatePromotion(promo)
		.success(function(data){
			console.log(data);

			//updated successfully
			if(data.status == 'success')
			{
				//notify use
				$scope.promotion = data.updated_promotion;

				$scope.setPreview($scope.promotion.shopify_theme_id);

				if(data.activated) ShopifyApp.flashNotice('Promo bar updated and activated successfully.');
				else ShopifyApp.flashNotice('Promo bar updated successfully.');

				//redirect back to home view
				$state.go('home', {});
			}
			else
			{
				console.log('failed to create the layout');

				ShopifyApp.flashError(data.message);
			}

		})
		.error(function(error){
			console.log(error);
		});
	}


	//show preview bar
    $scope.showPreview = function(){
    	var promo = getCurrentPromo();

    	if(promo)
    	{
    		APIService.getPreview(promo)
    		.success(function(data){
				console.log(data);

				//updated successfully
				if(data.status == 'success')
				{
					//notify use
					var snippet = data.snippet;
					$('#snippet_container').html(snippet);

					setTimeout(function(){
						$('#snippet_container').html('');
					}, 5000);

				}
				else
				{
					console.log('failed to create the layout');

					ShopifyApp.flashError(data.message);
				}

			})
			.error(function(error){
				console.log(error);
			});
    	}
    }



	$scope.deactivatePromotion = function() {

		var promo = {};
		promo.id = $scope.promotion.id;


		//deactivate a promotion
		APIService.deactivatePromotion(promo.id)
		.success(function(data){
			console.log(data);

			//updated successfully
			if(data.status == 'success')
			{
				//notify use
				$scope.promotion = data.deactivated_promotion;

				ShopifyApp.flashNotice('Promo bar deactivated successfully.');

				//redirect back to home view
				$state.go('home', {});
			}
			else
			{
				console.log('failed to create the layout');

				ShopifyApp.flashError(data.message);
			}

		})
		.error(function(error){
			console.log(error);
		});
	}
}];


//this controller will handle the builder for promos
/*TSB.controllers.ActivatePromoCtrl = ['$scope', '$rootScope', '$location', 'APIService', '$state', '$stateParams', '$timeout', 'promotions', function($scope, $rootScope, $location, APIService, $state, $stateParams, $timeout, promotions){
	//$scope.promotions = promotion.data.promotions;

	//console.log($scope.promotions);



	$scope.activatePromotion = function() {

		var theme_id = $('select[name="shopify_theme_id"]').val();
		var promotion_id = $('select[name="entrance_animation"]').val();

		//update layout using our service
		APIService.activatePromotion(promotion_id, theme_id)
		.success(function(data){
			console.log(data);

			//updated successfully
			if(data.status == 'success')
			{
				//notify use

				ShopifyApp.flashNotice('Promo bar activated successfully.');
			}
			else
			{
				console.log('failed to delete the promo');

				ShopifyApp.flashError(data.message);
			}

		})
		.error(function(error){
			console.log(error);
		});
	}
}];*/
