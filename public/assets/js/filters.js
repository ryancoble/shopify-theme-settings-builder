
//filter for output snippet names without spaces
TSB.filters.liquid_ready = function () {
    return function (value) {
        return (!value) ? '' : value.replace(/\s+/g, '_');
    };
};


TSB.filters.liquid_bind_statement = function () {
    return function (value) {
        return (!value) ? '' : '{{ ' + value + ' }}';
    };
};


TSB.filters.orderByIndex = function() {
  return function(items, key, reverse) {
    var filtered = [];
    
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    

    filtered.sort(function (a, b) {

    	var aIndex = parseInt(a.vindex);
    	var bIndex = parseInt(b.vindex);

    	if(aIndex > bIndex) return 1;
    	else if(aIndex < bIndex) return -1;
    	return 0;
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
};