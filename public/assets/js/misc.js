
//makes the sidebar and preview container always take up 100% of the view port
window.makeFullHeight = function()
{
	$(document).ready(function() {
	    setTimeout(function(){
            $(".sidebar").css({'height': $(document).height() - 56});
            $(".preview").css({'height': $(document).height() - 56});

            //$('body').fadeIn('slow');
            //$('#splash_screen').hide();
        }, 200);


        $(window).resize(function(){

            $(".sidebar").css({'height': $(document).height() - 56});
            $(".preview").css({'height': $(document).height() - 56});

        });


    });
}