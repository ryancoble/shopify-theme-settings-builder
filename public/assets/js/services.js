//service for authenticating users
TSB.services.AuthenticationService = ['$http', function($http){
	return {
		login: function(credentials){
			return $http.post('/auth/login', credentials);
		},
		logout: function(){
			return $http.get('/auth/logout');
		},
		isLoggedIn: false
	}
}];




//service for backend API
TSB.services.APIService = ['$http', function($http){

	var url_base = '/api';
	var file_url_base = '/api/files'

	var factory = {};

	factory.getData = function() {
		return $http.get(url_base + '/init');
	}

	factory.getFile = function(id) {
		return $http.get(file_url_base + '/' + id);
	}

	//get all files for the current shop
	factory.getFiles = function() {
		return $http.get(file_url_base);
	}


	factory.createFile = function(file) {
		return $http.post(file_url_base, {
			//"layout_template_id": template_id,
			"file": file
		});
	}


	factory.updateFile = function(file) {
		return $http.put(file_url_base + '/' + file.id, {
			"file": file
		});
	}

	factory.deleteFile = function(file_id) {
		return $http.delete(file_url_base + '/' + file_id);
	}

	factory.getShop = function() {
		return $http.get('/api/shop');
	}

	/*factory.getPreview = function(promo) {
		return $http.post(promotion_url_base + '/preview', {'promotion': promo});
	}

	factory.activatePromotion = function(promotion_id, theme_id) {
		return $http.post(promotion_url_base + '/' + promotion_id + '/publish', {'shopify_theme_id': theme_id});
	}

	factory.deactivatePromotion = function(promotion_id) {
		return $http.put(promotion_url_base + '/' + promotion_id + '/deactivate', {});
	}*/
	return factory;


}];

