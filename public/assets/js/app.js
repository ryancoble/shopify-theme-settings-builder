

//setup our routes
TSB.config.push(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

	//console.log($state);

	var partials_url = '/assets/partials/';

	$urlRouterProvider.otherwise('/');

	$stateProvider
	.state('list_files', { //the home state will list existing SettingsFiles for the shop with option to create a new promotion
		url: '/',
		templateUrl: partials_url + 'home.html',
		resolve: {
			files: function(APIService){
				return APIService.getFiles();
			}
		},
		controller: 'ListFilesCtrl'
	})
	.state('create_file', {
		url: '/files/create',
		templateUrl: partials_url + 'create.html',
		resolve: {
			shop: function(APIService){
				return APIService.getShop();
			}
		},
		controller: 'CreateFileCtrl'
	})
	.state('delete_section', {
		url: '/sections/:section_id/delete',
		templateUrl: partials_url + 'delete.html',
		controller: 'DeleteSectionCtrl'
	})
	.state('build_settings', {
		url: '/sections/:section_id/settings',
		templateUrl: partials_url + 'build.html',
		resolve: {
			section: function(APIService, $stateParams){
				var section_id = $stateParams.section_id;

				return APIService.getSection(section_id);
			}
		},
		controller: 'BuildSettingsCtrl'
	});

}]);


TSB.run = ['$window', '$rootScope', '$location', 'AuthenticationService', 'APIService', '$stateParams', function($window, $rootScope, $location, AuthenticationService, APIService, $stateParams) {



	$rootScope.$on('$stateChangeSuccess', function(event, next, current){

		window.makeFullHeight();

	});

	$rootScope.goBack = function() {
		$window.history.back();
	}
}];
