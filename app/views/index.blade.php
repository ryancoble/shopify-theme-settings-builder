<!doctype html>
<html lang="en">
<head>
	
    <title>Shopify Theme Settings Builder</title>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Shopify Theme Settings Builder">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link rel="apple-touch-icon" href="{{ asset('assets/img/apple-touch-icon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <!-- Shopfiy -->
    <script src="//cdn.shopify.com/s/assets/external/app.js"></script>
    <script>
    ShopifyApp.init({
        apiKey: '{{ Config::get('shopify.APP_API_KEY') }}',
        shopOrigin: 'https://{{ Session::get('shop') }}',
        debug: true
    });
    </script>
    <script>
    ShopifyApp.ready(function() {
        //ShopifyApp.flashNotice("Sucessfully loaded minions");
        ShopifyApp.Bar.initialize({
            icon: "{{ URL::asset('assets/img/rc_logo.png') }}",
            title: '{{ Session::get('shop') }} Homepage',
            buttons: {

                /*secondary:
                {
                    label: 'Add New',
                    callback: function() { $('.items').trigger('addnew'); }
                },*/

                /*primary:
                //[
                    {
                        label: 'Publish',
                        href: '',
                        //callback: APP.publish(),
                        target: 'app'
                    }
                //]*/

            }
        });
    });
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{ asset('assets/js/vendor/jquery.datetimepicker.js') }}"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <!-- our script includes -->
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-animate.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-route.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>

    <!-- Angular Booter -->
    <script src="{{ asset('assets/js/angularBooter.js') }}"></script>


    <base href="/">
	

</head>
<body data-ng-app="themeSettingsBuilder">

	<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <section id="secondary">
        <div class="col-md-1 col-lg-1 text-center" style="padding: 0;">
            <img class="animated fadeInLeft" src="{{ asset('/assets/img/rc_logo.png') }}" style="margin-left: 25px;">
        </div>
        <div class="col-md-11 col-lg-11 text-center" style="padding: 0;">
            <ul class="list-inline">
                <li class="animated fadeInRightBig" style="background-color: #F8BE00;"><a href="#"><img src="{{ asset('/assets/img/icon-menu.png') }}"> Mega Menu</a></li>
                <li class="animated fadeInRightBig" style="background-color: white; z-index: 2;"><a href="#"><img src="{{ asset('/assets/img/icon-homepage.png') }}"> Simple Promo Bar</a></li>
                <li class="animated fadeInRightBig"><a href="#"><img src="{{ asset('/assets/img/icon-homepage.png') }}"> Homepage Pro</a></li>
                <li class="coming-soon animated fadeInRightBig" style="background-color: #F8BE00;"><a href="#"><span>Coming Soon</span><img src="{{ asset('/assets/img/icon-products.png') }}"> Mega Products</a></li>
            </ul>
        </div>
    </section>

    <div class="main-view" data-autoscroll="false" ui-view=""></div>

    <script>

    var TSB = new AngularBooter('themeSettingsBuilder');

    //TSB.dependencies.push('ngAnimate');
    //TSB.dependencies.push('ngRoute');
    TSB.dependencies.push('ui.router');

    </script>

    <script src="{{ asset('assets/js/misc.js') }}"></script>

    <!-- angular app and its dependencies -->
    <script src="{{ asset('assets/js/services.js') }}"></script>
    <script src="{{ asset('assets/js/controllers.js') }}"></script>
    <script src="{{ asset('assets/js/filters.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>

    <script>
    //boot the app
    TSB.boot();
    
    </script>
</body>
</html>
