<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SettingFile extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'files';

	//get the theme for this settings file
	public function theme()
	{
		return $this->belongsTo('Theme');
	}

	//get all of the sections for the settings file
	public function sections()
	{
		return $this->hasMany('Section', 'id', 'section_id');
	}

}