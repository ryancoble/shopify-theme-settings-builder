<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Setting extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'settings';

	//get the settings section for this setting
	public function section()
	{
		return $this->belongsTo('Section', 'section_id', 'id');
	}


	public function metadata()
	{
		return $this->hasMany('SettingMetafield', 'id', 'setting_id');
	}

}