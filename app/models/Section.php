<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Section extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'sections';

	//get the file that this section belongs to
	public function file()
	{
		return $this->belongsTo('SettingFile', 'file_id', 'id');

	}

	//get all settings that belong to this section
	public function settings()
	{
		return $this->hasMany('Setting', 'id', 'setting_id');
	}
}