<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SettingsFile extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'settings_files';
	protected $fillable = ['name', 'shop_id'];

	//get the shop for this settings file
	public function shop()
	{
		return $this->belongsTo('Shop');
	}

	//get all of the sections for the settings file
	public function sections()
	{
		return $this->hasMany('SettingsSection', 'id', 'section_id');
	}

}