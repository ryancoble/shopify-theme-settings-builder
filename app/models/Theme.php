<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Theme extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'themes';


	public function shop()
	{
		return $this->belongsTo('Shop', 'shop_id', 'id');
	}

	//get the files that belong to this theme
	public function files()
	{
		return $this->hasMany('SettingFile', 'id', 'theme_id');
	}

	//get all settings that belong to this section
	/*public function settings()
	{
		return $this->hasMany('Setting', 'id', 'setting_id');
	}*/
}