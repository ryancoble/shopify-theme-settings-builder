<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SettingMetafield extends Eloquent {
	use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
	protected $table = 'settings_metafields';


	public function setting()
	{
		return $this->belongsTo('Setting', 'setting_id', 'id');
	}
}