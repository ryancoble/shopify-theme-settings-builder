<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Shops
		Schema::create('shops', function(Blueprint $t){
			$t->increments('id');
			$t->string('domain');
			$t->integer('UID');
			$t->string('access_token');
			$t->timestamps();
			$t->softDeletes();
		});

		//Themes
		Schema::create('themes', function(Blueprint $t) {
			$t->increments('id');
			$t->string('shopify_id');
			$t->integer('shop_id')->unsigned();
			$t->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
			$t->string('name');
			$t->string('role');
			$t->timestamps();
		});


		//Settings files
		Schema::create('files', function(Blueprint $t){
			$t->increments('id');
			$t->integer('theme_id')->unsigned();
			$t->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');
			$t->string('name');

			$t->timestamps();
			$t->softDeletes();
		});

		//Sections
		Schema::create('sections', function(Blueprint $t){
			$t->increments('id');
			$t->integer('file_id')->unsigned();
			$t->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
			$t->string('name');
			$t->integer('h_index');

			$t->timestamps();
			$t->softDeletes();
		});

		//Settings
		Schema::create('settings', function(Blueprint $t){
			$t->increments('id');
			$t->string('shopify_id');
			$t->integer('section_id')->unsigned();
			$t->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
			$t->integer('h_index');

			$t->timestamps();
			$t->softDeletes();
		});


		//Settings metafields
		Schema::create('settings_metafields', function(Blueprint $t){
			$t->increments('id');
			$t->integer('setting_id')->unsigned();
			$t->foreign('setting_id')->references('id')->on('settings')->onDelete('cascade');
			$t->text('value');

			$t->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Remove tables		
		Schema::drop('settings_metafields');
		Schema::drop('settings');
		Schema::drop('sections');
		Schema::drop('files');
		Schema::drop('themes');
		Schema::drop('shops');
	}

}

