<?php
namespace RocketCode\Shopify\Themes;

use RocketCode\Shopify\DbRepository;

class DbThemeRepository extends DbRepository implements ThemeRepository
{

	private $model;

	public function __construct(Theme $model)
	{
		$this->model = $model;
	}

}