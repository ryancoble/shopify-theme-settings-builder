<?php
namespace RocketCode\Shopify;

abstract class DbRepository
{
	public function getById($id)
	{
		return $this->model->find($id);
	}

	public function getAll()
	{
		return $this->model->get();
	}
}