<?php

# Cron job command for Laravel 4.2
# Inspired by Laravel 5's new upcoming scheduler (https://laravel-news.com/2014/11/laravel-5-scheduler)
#
# Author: Soren Schwert (GitHub: sisou)
#
# Requirements:
# =============
# PHP 5.4
# Laravel 4.2 ? (not tested with 4.1 or below)
# A desire to put all application logic into version control
#
# Installation:
# =============
# 1. Put this file into your app/commands/ directory and name it 'CronRunCommand.php'.
# 2. In your artisan.php file (found in app/start/), put this line: 'Artisan::add(new CronRunCommand);'.
# 3. On the server's command line, run 'php artisan cron:run'. If you see a message telling you the
#    execution time, it works!
# 4. On your server, configure a cron job to call 'php-cli artisan cron:run >/dev/null 2>&1' and to
#    run every five minutes (*/5 * * * *)
# 5. Observe your laravel.log file (found in app/storage/logs/) for messages starting with 'Cron'.
#
# Usage:
# ======
# 1. Have a look at the example provided in the fire() function.
# 2. Have a look at the available schedules below (starting at line 132).
# 4. Code your schedule inside the fire() function.
# 3. Done. Now go push your cron logic into version control!

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScheduleProcessCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'simplepromo:process';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Process all promo bars that have been scheduled both activation and deactivation.';

	/**
	 * Current timestamp when command is called.
	 *
	 * @var integer
	 */
	protected $timestamp;

	/**
	 * Hold messages that get logged
	 *
	 * @var array
	 */
	protected $messages = array();

	/**
	 * Specify the time of day that daily tasks get run
	 *
	 * @var string [HH:MM]
	 */
	protected $runAt = '03:00';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->timestamp = time();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$iTime = time();

		//loop all promotions
		$promotions = Promotion::all();
		foreach($promotions as $promo)
		{
			$this->messages[] = '(ID: ' . $promo->id . ') Checking promotion...';


			//if start or end date is set
			if(($promo->scheduled_start_date && $promo->scheduled_start_date != '0000-00-00 00:00:00') || ($promo->scheduled_end_date && $promo->scheduled_end_date != '0000-00-00 00:00:00'))
			{
				//check if the promo is active
				if($promo->is_active == 1)
				{
					//perform de-activation checks

					//end_date passed
					if($iTime >= strtotime($promo->scheduled_end_date))
					{
						$this->messages[] = '(ID: ' . $promo->id . ') Promotion de-activated, according to schedule. (' . strtotime($promo->scheduled_end_date) . ')';
						$this->setPromo($promo, false);
					}
				}
				else
				{
					//perform activation checks

					//check if both start and end time set
					if(($promo->scheduled_start_date && $promo->scheduled_start_date != '0000-00-00 00:00:00') && ($promo->scheduled_end_date && $promo->scheduled_end_date != '0000-00-00 00:00:00'))
					{
						//start and end time is set

						//if start_date passed and end_date is not passed
						if($iTime >= strtotime($promo->scheduled_start_date) && $iTime <= strtotime($promo->scheduled_end_date))
						{
							//Log::info(json_encode($promo));

							$this->messages[] = '(ID: ' . $promo->id . ') Promotion activated, according to schedule. (' . strtotime($promo->scheduled_end_date) . ')';
							$this->setPromo($promo, true);
						}
					}
					else
					{
						//end time not set

						//if start_date passed and end_date is not passed
						if($iTime >= strtotime($promo->scheduled_start_date))
						{
							$this->messages[] = '(ID: ' . $promo->id . ') Promotion activated, according to schedule. (' . strtotime($promo->scheduled_end_date) . ')';
							$this->setPromo($promo, true);
						}
					}
				}
			}

		}

		$this->finish();
	}


	private function setAllInactive($promotions, $promo_id)
	{
		//change all but currently active promo as inactive
		foreach($promotions as $p)
		{
			if($p->id !== $promo_id)
			{
				$p->is_active = 0;
				$p->save();
			}
			else
			{
				$p->is_active = 1;
				$p->save();
			}
		}
	}


	private function setPromo($promo, $active = false) 
	{

		if($active) {
			$shop = $promo->shop;

			$promotions = Promotion::where('shop_id', '=', $shop->id)->get();

			$this->setAllInactive($promotions, $promo->id);
			$this->activatePromotion($promo);
		}
		else $this->deactivatePromotion($promo);

		return ;
	}

	private function activatePromotion($promo)
	{
		$shop = $promo->shop;

		//upload / update snippet file on Shopify Theme
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{
			$promotion = $promo->toArray();

		    //generate a view for the snippet
			$snippet_view = View::make('snippet', array('promo' => $promotion))->render();

			//add / modify snippet to shop's current theme
		    $res = $sh->call([
		    	'URL' => '/admin/themes/' . $promo->shopify_theme_id . '/assets.json',
		    	'METHOD' => 'PUT',
		    	'FAILONERROR' => FALSE,
		    	'ALLDATA' => TRUE,
		    	'DATA' => array(
		    		'asset' => array(
		    			'key' => 'snippets/rc-simple-promo.liquid',
		    			'value' => $snippet_view
		    		)
		    	)
		    ]);

		    //get all theme assets
		    $templates = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json' ]);

		    $layout_files = [];

		    //get all layout files
			foreach($templates->assets as $asset)
			{
				if (strpos($asset->key, 'layout/') !== FALSE)
				{
					$layout_files[] = $asset;
				}
			}


			//all layout files
			foreach($layout_files as $asset)
			{
				$layoutCall = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json?asset[key]=' . $asset->key, 'ALLDATA' => TRUE, 'FAILONERROR' => FALSE ]);
				$layout = $layoutCall->asset->value;
		
				// check for snippet include link
				$liquidPattern = "{% include 'rc-simple-promo' %}";
				if (strpos($layout, $liquidPattern) == false) //not found already
				{
					$liquidLink = "\n\t{% include 'rc-simple-promo' %}\n\n";
					
					$pos = $this->getSnippetPosition($layout);

					//add liquid snippet
					if($pos >= 0) $layout = substr_replace($layout, $liquidLink, $pos, 0);
				}
			
				$layoutData = array('asset' => array());
				$layoutData['asset']['key'] = $asset->key;
				$layoutData['asset']['value'] = $layout;
				$sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => $layoutData ]);
				
			}


		    //Log::info(json_encode($res));
			
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}

		//Log::info(json_encode($res));
	}

	private function getSnippetPosition($layout)
	{
		$pos1 = strpos($layout, '<body');

		//first portion of the body tag
		if($pos1 >= 0)
		{
			$bodyFound = substr($layout, $pos1, strlen($layout));

			$pos2 = strpos($bodyFound, '>');

			if($pos2 >= 0)
			{
				return $pos1 + $pos2 + 5;
			}
		}

		return false;
	}

	//deactivate a promotion from a shopify theme
	public function deactivatePromotion($promo)
	{
		$shop = $promo->shop;

		if($promo->shop_id !== $shop->id) return Response::json(['status' => 'fail', 'message' => 'You are not the owner of this shop.']);

		//upload / update snippet file on Shopify Theme
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{

		    //get all theme assets
		    $templates = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json' ]);

		    $layout_files = [];

		    //get all layout files
			foreach($templates->assets as $asset)
			{
				if (strpos($asset->key, 'layout/') !== FALSE)
				{
					$layout_files[] = $asset;
				}
			}


			//all layout files
			foreach($layout_files as $asset)
			{
				$layoutCall = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json?asset[key]=' . $asset->key, 'ALLDATA' => TRUE, 'FAILONERROR' => FALSE ]);
				$layout = $layoutCall->asset->value;
		
				// check for snippet include link
				$liquidPattern = "\n\t{% include 'rc-simple-promo' %}\n\n";
				$layout = str_replace($liquidPattern, '', $layout);

				$pos = strpos($layout, $liquidPattern);
				if($pos === FALSE)
				{
					$promo->is_active = 0;
					$promo->save();
				}

			
				$layoutData = array('asset' => array());
				$layoutData['asset']['key'] = $asset->key;
				$layoutData['asset']['value'] = $layout;
				$sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => $layoutData ]);
				
			}
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}

		//Log::info(json_encode($res));

		//return success
		//return Response::json(['status' => 'success', 'deactivated_promotion' => $promo, 'shop' => $shop]);

	}

	protected function finish()
	{
		// Write execution time and messages to the log
		$executionTime = round(((microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000), 3);
		Log::info('Cron: execution time: ' . $executionTime . ' | ' . implode(', ', $this->messages));
	}

	/**
	 * AVAILABLE SCHEDULES
	 */

	protected function everyOneMinute(callable $callback)
	{
		if((int) date('i', $this->timestamp) % 1 === 0) call_user_func($callback);
	}

	protected function everyFiveMinutes(callable $callback)
	{
		if((int) date('i', $this->timestamp) % 5 === 0) call_user_func($callback);
	}

	protected function everyTenMinutes(callable $callback)
	{
		if((int) date('i', $this->timestamp) % 10 === 0) call_user_func($callback);
	}

	protected function everyFifteenMinutes(callable $callback)
	{
		if((int) date('i', $this->timestamp) % 15 === 0) call_user_func($callback);
	}

	protected function everyThirtyMinutes(callable $callback)
	{
		if((int) date('i', $this->timestamp) % 30 === 0) call_user_func($callback);
	}

	/**
	 * Called every full hour
	 */
	protected function hourly(callable $callback)
	{
		if(date('i', $this->timestamp) === '00') call_user_func($callback);
	}

	/**
	 * Called every hour at the minute specified
	 *
	 * @param  integer $minute
	 */
	protected function hourlyAt($minute, callable $callback)
	{
		if((int) date('i', $this->timestamp) === $minute) call_user_func($callback);
	}

	/**
	 * Called every day
	 */
	protected function daily(callable $callback)
	{
		if(date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	/**
	 * Called every day at the 24h-format time specified
	 *
	 * @param  string $time [HH:MM]
	 */
	protected function dailyAt($time, callable $callback)
	{
		if(date('H:i', $this->timestamp) === $time) call_user_func($callback);
	}

	/**
	 * Called every day at 12:00am and 12:00pm
	 */
	protected function twiceDaily(callable $callback)
	{
		if(date('h:i', $this->timestamp) === '12:00') call_user_func($callback);
	}

	/**
	 * Called every weekday
	 */
	protected function weekdays(callable $callback)
	{
		$days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
		if(in_array(date('D', $this->timestamp), $days) && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function mondays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Mon' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function tuesdays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Tue' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function wednesdays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Wed' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function thursdays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Thu' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function fridays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Fri' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function saturdays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Sat' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	protected function sundays(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Sun' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	/**
	 * Called once every week (basically the same as using sundays() above...)
	 */
	protected function weekly(callable $callback)
	{
		if(date('D', $this->timestamp) === 'Sun' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	/**
	 * Called once every week at the specified day and time
	 *
	 * @param  string $day  [Three letter format (Mon, Tue, ...)]
	 * @param  string $time [HH:MM]
	 */
	protected function weeklyOn($day, $time, callable $callback)
	{
		if(date('D', $this->timestamp) === $day && date('H:i', $this->timestamp) === $time) call_user_func($callback);
	}

	/**
	 * Called each month on the 1st
	 */
	protected function monthly(callable $callback)
	{
		if(date('d', $this->timestamp) === '01' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

	/**
	 * Called each year on the 1st of January
	 */
	protected function yearly(callable $callback)
	{
		if(date('m', $this->timestamp) === '01' && date('d', $this->timestamp) === '01' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
	}

}