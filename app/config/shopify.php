<?php

return array(
    'APP_API_KEY'		=> $_ENV['APP_API_KEY'],
    'APP_API_SECRET'	=> $_ENV['APP_API_SECRET'],
    'APP_API_SCOPE' => array('read_products', 'write_script_tags', 'write_themes', 'read_themes'),
    'API_ACTK_KEY'      => $_ENV['API_ACTK_KEY'],
);