<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//get the install url for the app
Route::get('/authURL/{url}', array(
	'uses' => 'ShopifyController@installURL'
));

//authorize the shopify shop / install the app to the shopify shop
Route::get('/auth', array(
	'uses' => 'ShopifyController@installOrAuthenticate'
));

//uninstall webhook callback
Route::post('/uninstall', array(
	'uses' => 'ShopifyController@uninstall'
));



//The app should be authenticated through Shopify, before this point
Route::group(array('before' => 'shopify.auth'), function(){

	//Index page, will load the angular app base
	Route::get('/', function()
	{
		return View::make('index');
	});


	/*
	 * Back-end API routes
	 */

	Route::get('/api/shop', array(
		'uses' => 'ShopifyController@getShop',
		'as' => 'get-shop'
	));

	//bind a promotion to a route, when needed
	Route::model('file', 'SettingsFile');

	//get a single settings file
	Route::get('/api/files/{file}', array(
		'uses' => 'ThemeSettingsController@getOne',
		'as' => 'get-file'
	));

	//get a list of settings files for the shop
	Route::get('/api/files', array(
		'uses' => 'ThemeSettingsController@getAll',
		'as' => 'all-files'
	));

	//create a new settings file for the theme
	Route::post('/api/files', array(
		'uses' => 'ThemeSettingsController@create',
		'as' => 'create-file'
	));

	//edit a settings file
	Route::put('/api/files/{file}', array(
		'uses' => 'ThemeSettingsController@edit',
		'as' => 'edit-file'
	));

	//publish the settings file to a store's theme on Shopify
	Route::post('/api/files/{file}/publish', array(
		'uses' => 'ThemeSettingsController@publish',
		'as' => 'publish-file'
	));

	//unpublish the settings file
	Route::put('/api/files/{file}/unpublish', array(
		'uses' => 'ThemeSettingsController@unpublish',
		'as' => 'unpublish-file'
	));

	//delete a settings file
	Route::delete('/api/files/{file}', array(
		'uses' => 'ThemeSettingsController@delete',
		'as' => 'delete-file'
	));


});

