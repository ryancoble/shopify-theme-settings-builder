<?php

//We have already been authenticated by this point.
//So there should be no reason to check if we have a shop.

class ThemeSettingsController extends Controller {

	//get the data for a single settings file
	public function getOne(SettingsFile $file)
	{
		$shop = $this->getShop();

		if($file->shop_id !== $shop->id) return Response::json(['status' => 'fail', 'message' => 'You are not the owner of this shop.']);

		return Response::json(['status' => 'success', 'shop' => $shop, 'file' => $file]);
	}

	//retrieve all settings files for a shop
	public function getAll()
	{
		$shop = $this->getShop();
		$settings_files = $shop->files;

		Log::info(json_encode($settings_files));

		if(!$settings_files) return Response::json(['status'=>'fail', 'message'=>'No files found yet, create one!']);

		//upload / update snippet file on Shopify Theme
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{

		    //get all theme assets
		    $theme_assets = $sh->call([ 'URL' => 'themes/' . '10359433' . '/assets.json' ]);

		    $config_files = [];

		    //get all layout files
			foreach($theme_assets->assets as $asset)
			{
				if (strpos($asset->key, 'config/') !== FALSE)
				{
					$config_files[] = $asset;
				}
			}

			Log::info(json_encode($config_files));
			//return;


			//all config files
			foreach($config_files as $asset)
			{
				if(strpos($asset->key, 'settings_schema.json') !== FALSE)
				{
					Log::info(json_encode($asset->key));

					$configCall = $sh->call([ 'URL' => 'themes/' . '10359433' . '/assets.json?asset[key]=' . $asset->key, 'ALLDATA' => TRUE, 'FAILONERROR' => FALSE ]);
					$settings_schema = $configCall->asset->value;

					//$settings_schema_new = json_encode("[{ 'name': 'Homepage', 'settings': [ {'type': 'text', 'id': 'testing', 'default': 'Test'} ] }]");

					$file = $settings_files[0];

					$settings_schema_new = $this->buildSettingsFile($file);
					
					Log::info($settings_schema_new);

					$settingsSchemaData = array('asset' => array());
					$settingsSchemaData['asset']['key'] = $asset->key;
					$settingsSchemaData['asset']['value'] = $settings_schema_new;
					$res = $sh->call([ 'URL' => 'themes/' . '10359433' . '/assets.json', 'METHOD' => 'PUT', 'DATA' => $settingsSchemaData ]);
				}
			}
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}
		
		Log::info(json_encode($res));

		return Response::json(['status'=>'success', 'files' => $settings_files]);
	}

	//create a settings file
	public function create()
	{
		$shop = $this->getShop();

		$json = Input::json();
		$file_json = $json->get('file');

		Log::info(json_encode($file_json));

		//if(!$promotion_json['foreground_color'] || !$promotion_json['background_color'] || !$promotion_json['text']) return Response::json(['status' => 'fail', 'message' => 'Fill in the required fields.']);

		$name = $file_json['name'];
		$sections = $file_json['sections'];
		
		$file = new SettingsFile;
		$file->shop_id = $shop->id;
		$file->name = $name;
		$file->save();


		foreach($sections as $section)
		{
			//create the sections
			$sect = new SettingsSection;
			$sect->file_id = $file->id;
			$sect->name = $section['name'];
			$sect->save();

			foreach($section['settings'] as $setting)
			{
				//create the settings with a section
				$set = new Setting;
				$set->type_id = $setting['type_id'];
				$set->accessible_id = $setting['accessible_id'];
				$set->section_id = $sect->id;
				$set->save();

			}
		}

		$new_file = SettingsFile::where('id', '=', $file->id)->with('settings_sections', 'settings');

		return Response::json(['status' => 'success', 'created_file' => $new_file]);
	}

	//delete a settings file
	public function delete(SettingsFile $file)
	{
		$shop = $this->getShop();


		if($file->shop_id !== $shop->id) return Response::json(['status' => 'fail', 'message' => 'You are not the owner of this shop.']);

		$file->delete();

		return Response::json(['status' => 'success', 'deleted_file' => $file]);
	}

	private function buildSettingsFile(SettingsFile $file)
	{
		$sections = $file->sections;

		$file_sections = array();

		//build each section
		foreach($sections as $section)
		{
			$file_sections[] = array_only((array)$section, array('name', 'settings'));
		}


		$settings_schema_new = array(
			$file_sections
		);

		return json_encode($settings_schema_new);

	}

	/*public function getPreview()
	{

		$json = Input::json();
		$promotion = $json->get('promotion');

		$promotion = (array) $promotion;

		$promotion['position'] = 'top';


		//Log::info(json_encode($promotion));


		$snippet = View::make('snippet', array('promo'=>$promotion))->render();

		return Response::json(['status'=>'success', 'snippet'=>$snippet]);
	}
	

	//deactivate a promotion from a shopify theme
	public function deactivate(Promotion $promo)
	{
		$shop = $this->getShop();

		if($promo->shop_id !== $shop->id) return Response::json(['status' => 'fail', 'message' => 'You are not the owner of this shop.']);

		//upload / update snippet file on Shopify Theme
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{

		    //get all theme assets
		    $templates = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json' ]);

		    $layout_files = [];

		    //get all layout files
			foreach($templates->assets as $asset)
			{
				if (strpos($asset->key, 'layout/') !== FALSE)
				{
					$layout_files[] = $asset;
				}
			}


			//all layout files
			foreach($layout_files as $asset)
			{
				$layoutCall = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json?asset[key]=' . $asset->key, 'ALLDATA' => TRUE, 'FAILONERROR' => FALSE ]);
				$layout = $layoutCall->asset->value;
		
				// check for snippet include link
				$liquidPattern = "\n\t{% include 'rc-simple-promo' %}\n\n";
				$layout = str_replace($liquidPattern, '', $layout);

				$pos = strpos($layout, $liquidPattern);
				if($pos === FALSE)
				{
					$promo->is_active = 0;
					$promo->save();
				}
				
			
				$layoutData = array('asset' => array());
				$layoutData['asset']['key'] = $asset->key;
				$layoutData['asset']['value'] = $layout;
				$sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => $layoutData ]);
				
			}
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}

		//return success
		return Response::json(['status' => 'success', 'deactivated_promotion' => $promo, 'shop' => $shop]);

	}*/



	//get the current shop
	private function getShop()
	{
		return Shop::where('domain', '=', Session::get('shop'))->with('files')->first();
	}


	/*private function activate(Promotion $promo)
	{
		$shop = $this->getShop();

		//upload / update snippet file on Shopify Theme
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{
			$promotion = $promo->toArray();

			//Log::info(json_encode($promotion));


		    //generate a view for the snippet
			$snippet_view = View::make('snippet', array('promo' => $promotion))->render();

			//add / modify snippet to shop's current theme
		    $res = $sh->call([
		    	'URL' => '/admin/themes/' . $promo->shopify_theme_id . '/assets.json',
		    	'METHOD' => 'PUT',
		    	'FAILONERROR' => FALSE,
		    	'ALLDATA' => TRUE,
		    	'DATA' => array(
		    		'asset' => array(
		    			'key' => 'snippets/rc-simple-promo.liquid',
		    			'value' => $snippet_view
		    		)
		    	)
		    ]);

		    //get all theme assets
		    $templates = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json' ]);

		    $layout_files = [];

		    //get all layout files
			foreach($templates->assets as $asset)
			{
				if (strpos($asset->key, 'layout/') !== FALSE)
				{
					$layout_files[] = $asset;
				}
			}


			//all layout files
			foreach($layout_files as $asset)
			{
				$layoutCall = $sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json?asset[key]=' . $asset->key, 'ALLDATA' => TRUE, 'FAILONERROR' => FALSE ]);
				$layout = $layoutCall->asset->value;
		
				// check for snippet include link
				$liquidPattern = "{% include 'rc-simple-promo' %}";
				if (strpos($layout, $liquidPattern) == false) //not found already
				{
					$liquidLink = "\n\t{% include 'rc-simple-promo' %}\n\n";
					
					$pos = $this->getSnippetPosition($layout);

					//add liquid snippet
					if($pos >= 0) {
						$layout = substr_replace($layout, $liquidLink, $pos, 0);

						$promotions = $shop->promotions;

						$this->setAllInactive($promotions, $promo->id);

					}
				}
			
				$layoutData = array('asset' => array());
				$layoutData['asset']['key'] = $asset->key;
				$layoutData['asset']['value'] = $layout;
				$sh->call([ 'URL' => 'themes/' . $promo->shopify_theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => $layoutData ]);
				
			}


		    //Log::info(json_encode($res));
			
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}

		//Log::info(json_encode($res));
	}

	private function getSnippetPosition($layout)
	{
		$pos1 = strpos($layout, '<body');

		//first portion of the body tag
		if($pos1 >= 0)
		{
			$bodyFound = substr($layout, $pos1, strlen($layout));

			$pos2 = strpos($bodyFound, '>');

			if($pos2 >= 0)
			{
				return $pos1 + $pos2 + 5;
			}
		}

		return false;
	}


	private function setAllInactive($promotions, $promo_id)
	{
		//change all but currently active promo as inactive
		foreach($promotions as $p)
		{
			if($p->id !== $promo_id)
			{
				$p->is_active = 0;
				$p->save();
			}
			else
			{
				$p->is_active = 1;
				$p->save();
			}
		}
	}*/




	

	//create a new a settings file by name

	//retrieve a settings file

	//update a settings file's name

	//delete a settings file

	//export a settings file

	//publish a settings file, to a shopify theme



	//add a new section to the settings file by name

	//retrieve a section

	//update a settings section's name

	//remove a section from the settings file

	//add a setting based on setting type, to a settings section

	//update a setting 

	//remove a setting





}
