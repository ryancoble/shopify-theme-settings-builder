<?php

class ShopifyController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Shopify Controller
	|--------------------------------------------------------------------------
	|
	*/


	/**
	 * Main Controller Method for Shopify Authorization
	 */
	public function installOrAuthenticate()
	{
		if (Input::get('code'))
		{
			// New install

			Log::info('New Install: ' . Input::get('shop'));
			$sh = App::make('ShopifyAPI', ['API_KEY'     => Config::get('shopify.APP_API_KEY'),
			                               'API_SECRET'  => Config::get('shopify.APP_API_SECRET'),
			                               'SHOP_DOMAIN' => Input::get('shop')
			]);

			// Get Access Token

			try
			{
				$accessToken = $sh->getAccessToken(Input::get('code'));
			}
			catch (Exception $e)
			{
				Log::error($e->getMessage());
				die('<pre>Error: ' . $e->getMessage() . '</pre>');
			}

			$shop = Shop::where('domain', Input::get('shop'))->first();

			if (!$shop)
			{
                 Log::info(__LINE__ . ': New Shop');
				$shop = new Shop;
			}

			$shop->setDomain(Input::get('shop'));
			$shop->setAccessToken($accessToken);
			$shop->save();

			$this->updateShopInfo($shop);


			/**
			 * Create webhook for uninstall
			 */
			$hookData = array('webhook' => array('topic' => 'app/uninstalled', 'address' => 'https://' . Request::server('HTTP_HOST') . '/uninstall-hook', 'format' => 'json'));
			try
			{
				$sh->setup(['ACCESS_TOKEN' => $shop->getAccessToken()]);
				$sh->call(['URL' => 'webhooks.json', 'METHOD' => 'POST', 'DATA' => $hookData]);
			}
			catch (Exception $e)
			{
				Log::error('Issue creating uninstall webhook - ' . $shop->domain . ' : ' . $e->getMessage());
			}

			Session::put('shop', $shop->domain);
			return Redirect::to('/');


		}
		else
		{
			// Accessing app from apps screen
			$shop = Shop::where('domain', Input::get('shop'))->first();
			if ($shop)
			{
				Log::info('Shop found after Auth: ' . Input::get('shop'));
				$this->updateShopInfo($shop);
				Session::put('shop', Input::get('shop'));

				return Redirect::to('/');
			}
			else
			{
				Log::warning('Shop redirecting to install: ' . Input::get('shop'));
                $sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'SHOP_DOMAIN' => Input::get('shop')]);
                return Redirect::to($sh->installURL(['permissions' => Config::get('shopify.APP_API_SCOPE')]));
			}
		}



	} // end check()

	/**
	 * Soft deletes shop from DB
	 * @return string
	 * @TODO Verify data
	 */
	public function uninstall()
	{

		$shop = Shop::where('domain', Input::get('myshopify_domain'))->firstOrFail();


        $shop->delete();
		return 'Thank You!';
	}

	public function installURL($shopURL = '')
	{
		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'SHOP_DOMAIN' => $shopURL]);
		return $sh->installURL(['permissions' => Config::get('shopify.APP_API_SCOPE')]);
	}



	public function getShop()
	{

		$shop = Shop::where('domain', '=', Session::get('shop'))->first();

		$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET')]);

		$sh->setup(['SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken()]);

		$res = '';
		try
		{
		    $res = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET', 'FAILONERROR' => FALSE, 'ALLDATA' => TRUE ]);

		    //Log::info(json_encode($res));

		    //loop all shop themes to get current theme id
		    $theme_id = 0;
		    foreach($res->themes as $theme) if($theme->role == "main") $current_theme = $theme;

		    
		}
		catch (Exception $e)
		{
		    $res = $e->getMessage();
		}

		return Response::json(['status' => 'success', 'themes' => $res, 'shop' => $shop]);
	}




	/**
	 * Updates several parameters on app install and on auth check
	 * @param null $shop
	 * @return bool
	 */
	private function updateShopInfo($shop = NULL)
	{
		try
		{
			$sh = App::make('ShopifyAPI', ['API_KEY' => Config::get('shopify.APP_API_KEY'), 'API_SECRET' => Config::get('shopify.APP_API_SECRET'), 'SHOP_DOMAIN' => $shop->domain, 'ACCESS_TOKEN' => $shop->getAccessToken() ]);
			$data = $sh->call(['URL' => 'shop.json']);

            // Generate UID if not present
            if (!$shop->UID)
            {
                $shop->UID = strtoupper(hash('ripemd160', $shop->domain . '||' . $shop->access_token));
            }

			if ($data)
			{
				Session::put('shopInfo', $data->shop);

				//$shop->restore();
				//$shop->last_login = date('Y-m-d H:i:s');
				$shop->setDomain($data->shop->myshopify_domain);
				/*$shop->shop_type = $data->shop->plan_name;
                $shop->admin_email = $data->shop->email;*/

				$shop->save();
			}
			return TRUE;
		}
		catch (Exception $e)
		{
			Log::error('[AuthController::updateShopInfo() - ' . $shop->domain . '] ' . $e->getMessage());
			return FALSE;
		}

	}

}